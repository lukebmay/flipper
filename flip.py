#!/usr/bin/env python3

import random


class FlipGame(object):

    def __init__(self, money=1000, bet=10, maxBet=100):
        self.money = money
        self.bet = bet
        self.maxBet = maxBet
        self.history = []
        self.total = 0
        self.heads = 0
        self.tails = 0
        self.maxAccumulation = money

    def flip(self):
        f = random.choice([-1, 1])
        self.history.append(f)
        self.total += f
        if f == 1:
            self.heads += 1
        else:
            self.tails += 1

        self.updateFinancials()
        self.adjustmentAlgorithm()

    def updateFinancials(self):
        last = self.history[-1]
        if last > 0:
            self.money += self.bet
        else:
            self.money -= self.bet
        if self.money > self.maxAccumulation:
            self.maxAccumulation = self.money

    def setBet(self, bet):
        bet = min(self.maxBet, self.money, bet)
        self.bet = bet

    def adjustmentAlgorithm(self):
        # last = self.history[-1]
        # if last == -1:
        #     self.setBet(self.bet/2)
        # else:
        #     self.setBet(10)
        return

    def printStatus(self):
        print("==== status ====")
        print("Heads  :", self.heads)
        print("Tails  :", self.tails)
        print("H/T Tot:", self.total)
        print("Games Played:", len(self.history))
        print("Max Accumulation:", self.maxAccumulation)
        print("Money           :", self.money)
        print("Bet             :", self.bet)


if __name__ == "__main__":
    fg = FlipGame()

    # while fg.money > 0:
    #     fg.flip()

    for i in range(1000000):
        fg.flip()

    fg.printStatus()

    print("==== Out of Money ====")
